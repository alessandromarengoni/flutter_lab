import 'package:flutter/material.dart';
import 'package:flutter_lab/app.dart';
import 'package:flutter_lab/config.dart';

void main() {
  setup(
    apiKey: 'AIzaSyAzkbCcYohDy82ysEPHNZAEPzEYbt8Oj7I',
    baseUrl: 'https://flutter-lab-ds-default-rtdb.firebaseio.com',
  );
  runApp(MyApp());
}
