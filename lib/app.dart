import 'package:flutter/material.dart';
import 'package:flutter_lab/pages/catalog/catalog_page.dart';
import 'package:flutter_lab/pages/home/home_page.dart';
import 'package:flutter_lab/pages/login/login_page.dart';
import 'package:flutter_lab/pages/videocall/video_call_page.dart';
import 'package:flutter_lab/providers/auth.dart';
import 'package:provider/provider.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => Auth(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Lab',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Consumer<Auth>(
          builder: (context, auth, child) {
            if (auth.state == AuthState.Autheticated) {
              return HomePage();
            }
            return LoginPage();
          },
        ),
        // home: HomePage(),
        routes: {
          CatalogPage.routeName: (context) => CatalogPage(),
          VideoCallPage.routeName: (context) => VideoCallPage(),
        },
      ),
    );
  }
}
