import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_lab/config.dart';
import 'package:flutter_lab/models/filter.dart';
import 'package:flutter_lab/models/product.dart';
import 'package:http/http.dart' as http;

class Catalog extends ChangeNotifier {
  final AppConfig config = getIt<AppConfig>();
  List<Product> productList = [];
  List<Filter> filterList = [];
  bool loading;

  Future<void> loadPage() async {
    try {
      loading = true;
      notifyListeners();
      await getProductList();
      await getFilters();
    } catch (error) {
      print(error);
    } finally {
      loading = false;
      notifyListeners();
    }
  }

  Future<void> getProductList() async {
    try {
      final response = await http.get('${config.baseUrl}/products.json?auth=${config.token}');
      List<dynamic> data = json.decode(response.body);
      productList = data.map((item) => Product.fromMap(item)).toList();
    } catch (error) {
      print(error);
    }
  }

  Future<void> getFilters() async {
    try {
      final response = await http.get('${config.baseUrl}/filters.json?auth=${config.token}');
      List<dynamic> data = json.decode(response.body);
      filterList = data.map((item) => Filter.fromMap(item)).toList();
    } catch (error) {
      print(error);
    }
  }
}
