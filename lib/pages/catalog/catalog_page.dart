@JS()
library togetherJs;

import 'package:breakpoint/breakpoint.dart';
import 'package:flutter/material.dart';
import 'package:flutter_lab/models/product.dart';
import 'package:flutter_lab/pages/catalog/catalog.dart';
import 'package:provider/provider.dart';
import 'package:js/js.dart';

@JS('TogetherJS')
external dynamic togetherJS();

class CatalogPage extends StatelessWidget {
  static final String routeName = '/catalog';

  @override
  Widget build(BuildContext context) {
    final Catalog catalog = Catalog();
    return Scaffold(
      appBar: AppBar(
        title: Text('Catalogo'),
        // actions: [
        //   PopupMenuButton(
        //     icon: Icon(Icons.more_vert),
        //     itemBuilder: (context) => [
        //       PopupMenuItem(
        //         value: 1,
        //         child: Text('Start TogetherJS'),
        //       ),
        //     ],
        //     onSelected: (value) {
        //       togetherJS();
        //     },
        //   )
        // ],
      ),
      body: ChangeNotifierProvider<Catalog>.value(
        value: catalog,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: FutureBuilder(
            future: catalog.loadPage(),
            builder: (context, snapshot) {
              return LayoutBuilder(
                builder: (context, constraints) {
                  final _breakpoint = Breakpoint.fromConstraints(constraints);
                  return Consumer<Catalog>(
                    builder: (context, catalog, _) {
                      if (catalog.loading) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      return Column(
                        children: [
                          Wrap(
                            children: catalog.filterList
                                .map((filter) => DropdownButton(
                                      hint: Text(filter.name),
                                      items: filter.values
                                          .map((filterValue) => DropdownMenuItem(
                                                value: filterValue.value,
                                                child: Text(filterValue.label),
                                              ))
                                          .toList(),
                                      onChanged: (value) {},
                                    ))
                                .toList(),
                          ),
                          Expanded(
                            child: GridView.builder(
                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: _breakpoint.columns ~/ 2, crossAxisSpacing: _breakpoint.gutters / 2, mainAxisSpacing: _breakpoint.gutters / 2, childAspectRatio: 2 / 3),
                              itemCount: catalog.productList.length,
                              itemBuilder: (context, index) {
                                Product product = catalog.productList[index];
                                return Card(
                                  child: Column(
                                    children: [
                                      Expanded(
                                        child: Image.network(
                                          product.image[0],
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Text(product.name),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Text(product.prize.toStringAsFixed(2)),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      );
                    },
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }
}
