import 'package:flutter/material.dart';
import 'package:flutter_lab/pages/login/login.dart';
import 'package:provider/provider.dart';

class LoginForm extends StatefulWidget {
  LoginForm({@required Key key}) : super(key: key);

  @override
  LoginFormState createState() => LoginFormState();
}

class LoginFormState extends State<LoginForm> {
  TextEditingController _emailTextEditingController;
  TextEditingController _passwordTextEditingController;
  TextEditingController _confirmPasswordTextEditingController;
  GlobalKey<FormState> _formKey;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    _emailTextEditingController = TextEditingController();
    _passwordTextEditingController = TextEditingController();
    _confirmPasswordTextEditingController = TextEditingController();
  }

  bool validate() {
    return _formKey.currentState.validate();
  }

  void submit(void Function(String email, String password) onSubmit) {
    // _formKey.currentState.save();
    onSubmit(_emailTextEditingController.text, _passwordTextEditingController.text);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: _emailTextEditingController,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(labelText: 'Email'),
                validator: (value) {
                  if (value.isEmpty ||
                      !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?").hasMatch(value)) {
                    return 'Inserire una mail valida.';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 20.0,
              ),
              TextFormField(
                controller: _passwordTextEditingController,
                obscureText: true,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(labelText: 'Password'),
                validator: (value) {
                  if (value.length < 6) return 'La password deve essere di alemeno 6 caratteri';
                  return null;
                },
              ),
              Consumer<Login>(
                builder: (context, login, _) {
                  _confirmPasswordTextEditingController.text = '';
                  return login.createNew
                      ? Column(
                          children: [
                            SizedBox(
                              height: 20.0,
                            ),
                            TextFormField(
                              controller: _confirmPasswordTextEditingController,
                              obscureText: true,
                              keyboardType: TextInputType.visiblePassword,
                              decoration: InputDecoration(labelText: 'Ripeti password'),
                              validator: (value) {
                                if (value != _passwordTextEditingController.text) return 'Le password devono coincidere';
                                return null;
                              },
                            ),
                          ],
                        )
                      : Container();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
