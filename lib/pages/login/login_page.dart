import 'package:flutter/material.dart';
import 'package:flutter_lab/pages/login/login.dart';
import 'package:flutter_lab/pages/login/login_form.dart';
import 'package:flutter_lab/providers/auth.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    GlobalKey<LoginFormState> key = GlobalKey<LoginFormState>();
    return ChangeNotifierProvider<Login>(
      create: (context) => Login(Provider.of<Auth>(context, listen: false)),
      child: Scaffold(
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              SizedBox(
                height: 150.0,
              ),
              LoginForm(
                key: key,
              ),
              SizedBox(
                height: 50.0,
              ),
              Consumer<Login>(
                builder: (context, login, child) {
                  if (login.authenticating) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  final buttons = Column(
                    children: [
                      RaisedButton(
                        color: Theme.of(context).primaryColor,
                        child: Text(login.createNew ? 'SIGN UP' : 'SIGN IN'),
                        onPressed: () {
                          if (key.currentState.validate()) {
                            key.currentState.submit((email, password) async {
                              await (login.createNew ? login.signUp(email, password) : login.signIn(email, password));
                            });
                          }
                        },
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      RaisedButton(
                        child: Text(login.createNew ? 'AUTENTICATI' : 'REGISTRATI'),
                        onPressed: () {
                          login.switchMode();
                        },
                      ),
                    ],
                  );
                  return login.errorMessage != null
                      ? Column(
                          children: [
                            buttons,
                            SizedBox(
                              height: 20.0,
                            ),
                            Text(
                              login.errorMessage,
                              style: TextStyle(color: Colors.red.shade800),
                            ),
                          ],
                        )
                      : buttons;
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
