import 'package:flutter/material.dart';
import 'package:flutter_lab/providers/auth.dart';

class Login extends ChangeNotifier {
  final Auth auth;
  bool _authenticating = false;
  String errorMessage;
  bool _createNew = false;

  Login(this.auth);

  bool get authenticating => _authenticating;

  bool get createNew => _createNew;

  switchMode() {
    _createNew = !_createNew;
    errorMessage = null;
    notifyListeners();
  }

  Future<void> signIn(String email, String password) async {
    _authenticating = true;
    notifyListeners();
    final result = await auth.signIn(email, password);
    if (result == AuthResponse.EmailNotFound) errorMessage = 'Email non trovata';
    if (result == AuthResponse.InvalidPassword) errorMessage = 'Password non valida';
    if (result == AuthResponse.GenericError) errorMessage = 'Errore durante la login';
    _authenticating = false;
    notifyListeners();
  }

  Future<void> signUp(String email, String password) async {
    _authenticating = true;
    notifyListeners();
    final result = await auth.signUp(email, password);
    if (result == AuthResponse.EmailExists) errorMessage = 'Esiste già un utente con questa mail';
    _authenticating = false;
    notifyListeners();
  }
}
