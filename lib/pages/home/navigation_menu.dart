import 'package:breakpoint/breakpoint.dart';
import 'package:flutter/material.dart';
import 'package:flutter_lab/pages/catalog/catalog_page.dart';
import 'package:flutter_lab/pages/videocall/video_call_page.dart';

class NavigationMenuItem {
  String title;
  IconData icon;
  Function(BuildContext context) onSelect;

  NavigationMenuItem({
    this.title,
    this.icon,
    this.onSelect,
  });
}

class NavigationMenu extends StatelessWidget {
  final List<NavigationMenuItem> items = [
    NavigationMenuItem(
      title: 'Catalogo',
      icon: Icons.shopping_basket,
      onSelect: (BuildContext context) {
        Navigator.of(context).pushNamed(CatalogPage.routeName);
      },
    ),
    NavigationMenuItem(
      title: 'Novità',
      icon: Icons.star,
      onSelect: (BuildContext context) {},
    ),
    NavigationMenuItem(
      title: 'Promozioni',
      icon: Icons.local_offer,
      onSelect: (BuildContext context) {},
    ),
    NavigationMenuItem(
      title: 'Experience',
      icon: Icons.wb_sunny,
      onSelect: (BuildContext context) {
        Navigator.of(context).pushNamed(VideoCallPage.routeName);
      },
    ),
  ];

  List<Widget> _buildRowItems(Size screenSize, BuildContext context) {
    List<Widget> rowItems = [];
    for (int i = 0; i < items.length; i++) {
      Widget element = InkWell(
        onTap: () {
          items[i].onSelect(context);
        },
        child: Text(items[i].title),
      );
      Widget spacer = SizedBox(
        height: screenSize.height / 20,
        child: VerticalDivider(
          width: 1,
          thickness: 1,
          color: Colors.black,
        ),
      );
      rowItems.add(element);
      if (i < items.length - 1) {
        rowItems.add(spacer);
      }
    }
    return rowItems;
  }

  @override
  Widget build(BuildContext context) {
    final _breakpoints = Breakpoint.fromMediaQuery(context);
    final _screenSize = MediaQuery.of(context).size;
    if (_breakpoints.window == WindowSize.small || _breakpoints.window == WindowSize.xsmall) {
      return Column(
        children: items
            .map((item) => Padding(
                  padding: EdgeInsets.only(top: _screenSize.height / 80),
                  child: Card(
                    child: InkWell(
                      onTap: () {
                        item.onSelect(context);
                      },
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: _screenSize.height / 40,
                          bottom: _screenSize.height / 40,
                          left: _screenSize.width / 20,
                        ),
                        child: Row(
                          children: [
                            Icon(item.icon),
                            SizedBox(
                              width: _screenSize.width / 20,
                            ),
                            Text(item.title),
                          ],
                        ),
                      ),
                    ),
                  ),
                ))
            .toList(),
      );
    }
    return Card(
      child: Padding(
        padding: EdgeInsets.all(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: _buildRowItems(_screenSize, context),
        ),
      ),
    );
  }
}
