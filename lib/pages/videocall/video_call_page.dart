import 'package:flutter/material.dart';
import 'package:flutter_lab/pages/videocall/video_call_room.dart';

class VideoCallPage extends StatefulWidget {
  static final String routeName = '/video-call';
  @override
  _VideoCallPageState createState() => _VideoCallPageState();
}

class _VideoCallPageState extends State<VideoCallPage> {
  String name;
  bool connect = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Video Call'),
      ),
      body: !connect
          ? Center(
              child: Container(
                constraints: BoxConstraints(maxWidth: 300.0),
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextField(
                          decoration: InputDecoration(labelText: 'Inserisici un nome'),
                          onChanged: (text) {
                            setState(() {
                              name = text;
                            });
                          },
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        RaisedButton(
                          child: Text('CONNETTI'),
                          onPressed: (name != null && name.isNotEmpty)
                              ? () {
                                  setState(() {
                                    connect = true;
                                  });
                                }
                              : null,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
          : VideoCallRoom(name),
    );
  }
}
