import 'package:flutter/material.dart';
import 'package:flutter_lab/webrtc/signaling.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';

class VideoCallRoom extends StatefulWidget {
  final String name;

  VideoCallRoom(this.name);

  @override
  _VideoCallRoomState createState() => _VideoCallRoomState();
}

class _VideoCallRoomState extends State<VideoCallRoom> {
  Signaling signaling;
  List<dynamic> peers = [];
  dynamic selfId;
  RTCVideoRenderer localRenderer = RTCVideoRenderer();
  RTCVideoRenderer remoteRenderer = RTCVideoRenderer();
  bool inCalling = false;
  Session session;

  @override
  void initState() {
    _connect();
    super.initState();
  }

  void _connect() async {
    await localRenderer.initialize();
    await remoteRenderer.initialize();
    if (signaling == null) {
      signaling = Signaling(widget.name)..connect();

      signaling.onSignalingStateChange = (SignalingState state) {
        switch (state) {
          case SignalingState.ConnectionClosed:
          case SignalingState.ConnectionError:
          case SignalingState.ConnectionOpen:
            break;
        }
      };

      signaling.onCallStateChange = (Session session, CallState state) {
        switch (state) {
          case CallState.CallStateNew:
            setState(() {
              session = session;
              inCalling = true;
            });
            break;
          case CallState.CallStateBye:
            print('by');
            setState(() {
              localRenderer.srcObject = null;
              remoteRenderer.srcObject = null;
              inCalling = false;
              session = null;
            });
            break;
          case CallState.CallStateInvite:
          case CallState.CallStateConnected:
          case CallState.CallStateRinging:
        }
      };

      signaling.onPeersUpdate = ((event) {
        setState(() {
          selfId = event['self'];
          peers = event['peers'];
        });
      });

      signaling.onLocalStream = ((_, stream) {
        localRenderer.srcObject = stream;
      });

      signaling.onAddRemoteStream = ((_, stream) {
        remoteRenderer.srcObject = stream;
      });

      signaling.onRemoveRemoteStream = ((_, stream) {
        remoteRenderer.srcObject = null;
      });
    }
  }

  _invitePeer(BuildContext context, String peerId, bool useScreen) async {
    if (signaling != null && peerId != selfId) {
      signaling.invite(peerId, 'video', useScreen);
    }
  }

  _hangUp() {
    if (signaling != null) {
      print('_hangUp');
      signaling.bye(session.sid);
    }
  }

  _muteMic() {
    signaling.muteMic();
  }

  _buildRow(context, peer) {
    var self = (peer['id'] == selfId);
    return ListTile(
      title: Text(self ? peer['name'] + '[Your self]' : peer['name']),
      onTap: null,
      trailing: SizedBox(
          width: 100.0,
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
            IconButton(
              icon: const Icon(Icons.videocam),
              onPressed: () => _invitePeer(context, peer['id'], false),
              tooltip: 'Video calling',
            ),
            IconButton(
              icon: const Icon(Icons.screen_share),
              onPressed: () => _invitePeer(context, peer['id'], true),
              tooltip: 'Screen sharing',
            )
          ])),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: inCalling
          ? SizedBox(
              width: 200.0,
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
                FloatingActionButton(
                  onPressed: _hangUp,
                  tooltip: 'Hangup',
                  child: Icon(Icons.call_end),
                  backgroundColor: Colors.pink,
                ),
                FloatingActionButton(
                  child: const Icon(Icons.mic_off),
                  onPressed: _muteMic,
                )
              ]))
          : null,
      body: inCalling
          ? Container(
              child: Stack(children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: RTCVideoView(remoteRenderer),
                  decoration: BoxDecoration(color: Colors.black54),
                ),
                Positioned(
                  left: 20.0,
                  top: 20.0,
                  child: Container(
                    width: 120.0,
                    height: 90.0,
                    child: RTCVideoView(localRenderer, mirror: true),
                    decoration: BoxDecoration(color: Colors.black54),
                  ),
                ),
              ]),
            )
          : ListView.builder(
              shrinkWrap: true,
              // padding: const EdgeInsets.all(0.0),
              itemCount: peers.length,
              itemBuilder: (context, index) {
                return _buildRow(context, peers[index]);
              }),
    );
  }
}
