import 'package:flutter/material.dart';
import 'package:flutter_lab/app.dart';
import 'package:flutter_lab/config.dart';

void main() {
  setup(
    apiKey: 'AIzaSyCyNxXVB264uyphgcvdz6V-5qAtY-Gi4Xg',
    baseUrl: 'https://flutter-lab-ds-dev-default-rtdb.firebaseio.com',
  );
  runApp(MyApp());
}
