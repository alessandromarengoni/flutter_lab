import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:flutter_lab/config.dart';
import 'package:http/http.dart' as http;

enum AuthState {
  Autheticated,
  Unauthenticated,
}

enum AuthResponse {
  Success,
  EmailNotFound,
  EmailExists,
  InvalidPassword,
  GenericError,
}

class Auth extends ChangeNotifier {
  final AppConfig config = getIt<AppConfig>();
  AuthState state = AuthState.Unauthenticated;

  Future<AuthResponse> signIn(String email, String password) async {
    AuthResponse result = AuthResponse.Success;
    try {
      final response = await http.post(
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${config.apiKey}',
        body: json.encode({
          'email': email,
          'password': password,
          'returnSecureToken': true,
        }),
        headers: {'Content-Type': 'application/json'},
      );
      final Map<String, dynamic> data = json.decode(response.body);
      if (response.statusCode == 200) {
        state = AuthState.Autheticated;
        config.token = data['idToken'];
        notifyListeners();
      } else {
        result = AuthResponse.GenericError;
        if (data['error']['message'] == 'EMAIL_NOT_FOUND') result = AuthResponse.EmailNotFound;
        if (data['error']['message'] == 'INVALID_PASSWORD') result = AuthResponse.InvalidPassword;
      }
    } catch (error) {
      print(error);
      result = AuthResponse.GenericError;
    }
    return result;
  }

  Future<AuthResponse> signUp(String email, String password) async {
    AuthResponse result = AuthResponse.Success;
    try {
      final response = await http.post(
        'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${config.apiKey}',
        body: json.encode({
          'email': email,
          'password': password,
          'returnSecureToken': true,
        }),
        headers: {'Content-Type': 'application/json'},
      );
      final Map<String, dynamic> data = json.decode(response.body);
      if (response.statusCode == 200) {
        state = AuthState.Autheticated;
        config.token = data['idToken'];
        notifyListeners();
      } else {
        result = AuthResponse.GenericError;
        if (data['error']['message'] == 'EMAIL_EXISTS') result = AuthResponse.EmailExists;
      }
    } catch (error) {
      print(error);
      result = AuthResponse.GenericError;
    }
    return result;
  }

  Future<void> logout() {
    config.token = null;
    state = AuthState.Unauthenticated;
    notifyListeners();
  }
}
