import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

void setup({String apiKey, String baseUrl}) {
  getIt.registerSingleton<AppConfig>(AppConfig(baseUrl: baseUrl, apiKey: apiKey));
}

class AppConfig {
  String baseUrl;
  String apiKey;
  String token;

  AppConfig({this.baseUrl, this.apiKey});
}
