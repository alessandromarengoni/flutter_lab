class FilterValue {
  String label;
  dynamic value;
  bool selected = false;

  FilterValue({
    this.label,
    this.value,
    this.selected,
  });

  FilterValue.fromMap(Map<String, dynamic> map)
      : this.label = map['label'],
        this.value = map['value'];
}

class Filter {
  String name;
  String attribute;
  List<FilterValue> values;

  Filter({
    this.name,
    this.attribute,
    this.values,
  });

  Filter.fromMap(Map<String, dynamic> map)
      : this.name = map['name'],
        this.attribute = map['attribute'],
        this.values = (map['values'] as List).map((map) => FilterValue.fromMap(map)).toList();
}
