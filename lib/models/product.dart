final Map<String, String> colorMap = {
  "BIANCO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/6116090906/001/s3swatch/SW-6116090906001.jpg",
  "SABBIA": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/1016150306/002/s3swatch/SW-1016150306002.jpg",
  "BLU MARINO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/1016150306/005/s3swatch/SW-1016150306005.jpg",
  "NERO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/9016110306/006/s3swatch/SW-9016110306006.jpg",
  "CUOIO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/1016150306/014/s3swatch/SW-1016150306014.jpg",
  "CAMMELLO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/1086020606/001/s3swatch/SW-1086020606001.jpg",
  "GHIACCIO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/9016110306/011/s3swatch/SW-9016110306011.jpg",
  "BEIGE": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/4516300706/002/s3swatch/SW-4516300706002.jpg",
  "CIOCCOLATO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/4516300706/003/s3swatch/SW-4516300706003.jpg",
  "BLUETTE": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/1116060906/032/s3swatch/SW-1116060906032.jpg",
  "CIPRIA": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/2116030306/001/s3swatch/SW-2116030306001.jpg",
  "SKIN": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/6116080906/002/s3swatch/SW-6116080906002.jpg",
  "ROSSO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/6116090906/004/s3swatch/SW-6116090906004.jpg",
  "VERDE": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/9136010806/006/s3swatch/SW-9136010806006.jpg",
  "GIALLO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/9136010806/007/s3swatch/SW-9136010806007.jpg",
  "BLU NOTTE": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/9106050906/001/s3swatch/SW-9106050906001.jpg",
  "CAMMELLO CHIARO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/5106040906/002/s3swatch/SW-5106040906002.jpg",
  "VERDE SMERALDO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/1226040406/010/s3swatch/SW-1226040406010.jpg",
  "ROSA": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/1226010406/013/s3swatch/SW-1226010406013.jpg",
  "FUXIA": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/1946020906/007/s3swatch/SW-1946020906007.jpg",
  "ORO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/1946040306/003/s3swatch/SW-1946040306003.jpg",
  "AVORIO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/5366200306/003/s3swatch/SW-5366200306003.jpg",
  "MARRONE": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/6366270906/010/s3swatch/SW-6366270906010.jpg",
  "BORDEAUX": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/4526090706/070/s3swatch/SW-4526090706070.jpg",
  "GRIGIO MEDIO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/4796010706/008/s3swatch/SW-4796010706008.jpg",
  "FANGO": "https://b2c-media.maxmara.com/sys-master/m0/MM/2020/2/4796010706/018/s3swatch/SW-4796010706018.jpg"
};

class Product {
  String name;
  String description;
  List<String> image;
  double prize;
  String category;
  String composition;
  List<String> sizes;
  List<String> colors;
  String material;
  String collecion;
  String productCode;
  bool favorite;
  String look;

  Product({
    this.category,
    this.description,
    this.image,
    this.name,
    this.prize,
  });

  Product.fromMap(Map<String, dynamic> map)
      : this.name = map['nome prodotto'],
        this.description = map['descrizione prodotto'],
        this.category = map['categoria'],
        this.image = (map['Link immagine'] as List).cast<String>(),
        this.prize = double.parse(map['prezzo']),
        this.collecion = map['collezione'],
        this.composition = map['composizione e lavaggio'],
        this.sizes = map['taglia'] != null ? (map['taglia'] as List).cast<String>() : [],
        this.colors = map['colore'] != null ? (map['colore'] as List).cast<String>() : [],
        this.material = map['materiale'],
        this.productCode = map['codice prodotto'],
        this.favorite = map['favorite'],
        this.look = map['look'];

  Map<String, dynamic> toMap() => {
        'nome prodotto': this.name,
        'descrizione prodotto': this.description,
        'categoria': this.category,
        'Link immagine': this.image,
        'prezzo': this.prize.toString(),
        'collezione': this.collecion,
        'composizione e lavaggio': this.composition,
        'taglia': this.sizes,
        'colore': this.colors,
        'codice prodotto': this.productCode,
        'materiale': this.material,
        'favorite': this.favorite,
        'look': this.look,
      };
}
